﻿using System;
using SevenTiny.Bantina.Security;

namespace console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello RSA!");
            var rs = RSACommon.Encrypt("Thach Tran Nhat Quang", "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuTDrJX8ArZc6AvRmlcbwHoeiOWDpCJ09Www13yxCM6a01yIIX9WiDhOOsHwSO9LdjHnOzq6dZMTeZ1eUMEnKsGBrYmlAjCaXvL9R4rANG2SDEYu1/8VMWgB0ik6oBk3MoiU9+f6h9ZpF0zjNBIhPtm6/InG0/586QbcD+EEPAhQ3+wYL7TnJLETlfB4FMUlEZWjwLO6YctCwX6ce+9MYt0KMDaZiIWfONkveZuyXaVvAzr55/p6YfCEmWqVGEr+2ZZEmL6RvtfpoXD/bIEdkQK71oe8BN5sCxECEyaAvFHEto9KMeXLL6WgNs65FhXyaV88OqTaw043Cf3GH/lOK6QIDAQAB");
            Console.WriteLine(rs);
        }
    }
}
